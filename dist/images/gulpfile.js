const { src, dest, series, watch, parallel } = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const csso = require('gulp-csso')
const del = require('del')
const autoprefixer = require('gulp-autoprefixer')
const concat = require('gulp-concat')
const purgecss = require('gulp-purgecss')
const uglify = require('gulp-uglify-es').default
const imagemin = require('gulp-imagemin')
const sync = require('browser-sync').create()

function html() {
  return src('src/index.html')
    .pipe(concat('index.html'))
    .pipe(dest('dist'))
    .pipe(sync.stream())
}

function minimg() {
  return src('src/imgs/**/*')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
        }),
      ]),
    )
    .pipe(dest('dist/images'))
}

function browserSync() {
  sync.init({
    server: {
      baseDir: 'dist/',
    },
  })
}

function cleanDist() {
  return del('dist')
}

function styles() {
  return src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(csso())
    .pipe(concat('styles.min.css'))
    .pipe(
      autoprefixer({
        overrideBrowserlist: ['last 5 versions'],
        grid: true,
      }),
    )
    .pipe(
      purgecss({
        content: ['src/index.html'],
      }),
    )
    .pipe(dest('./dist'))
    .pipe(sync.stream())
}

function scripts() {
  return src('src/js/add.js')
    .pipe(concat('add.min.js'))
    .pipe(uglify())
    .pipe(dest('dist'))
    .pipe(sync.stream())
}

function watching() {
  watch(['src/scss/**/*.scss'], styles)
  watch(['src/js/add.js'], scripts)
  watch(['src/*.html'], html)
}

exports.styles = styles
exports.scripts = scripts
exports.watching = watching
exports.browserSync = browserSync
exports.minimg = minimg
exports.cleanDist = cleanDist

exports.build = series(cleanDist, minimg, html, styles, scripts)
exports.dev = parallel(styles, html, scripts, browserSync, watching)
