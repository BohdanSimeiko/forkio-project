const menuBtn = document.querySelector(".mobile_menu_btn.disactive-btn");
const menu = document.querySelector(".drop_menu.disactive");

menuBtn.onclick = () => {
  menuBtn.classList.toggle("disactive-btn");
  menu.classList.toggle("disactive");
};
